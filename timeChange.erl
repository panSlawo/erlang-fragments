-module(timeChange).
-export([addTime/2,
         formatTime/1]).


%%---------------------------------------------
%% -spec
%% addTime(AddMinutes) -> Result,
%% AddMinutes = integer() ([0 ... 25]),
%% AddSeconds = integer() ([0 ... 59]),
%% Result = datetime() | {error, Reason}.
%%---------------------------------------------
addTime(AddMinutes,AddSeconds) ->

   AddTime = AddMinutes * 60 + AddSeconds,

   if
      AddTime > 1559 -> error(1);
      true -> ok
   end,

   CurrentTime = erlang:localtime(),
   CurrentSeconds = calendar:datetime_to_gregorian_seconds(CurrentTime),
   ChangedSeconds = CurrentSeconds + AddTime,
   calendar:gregorian_seconds_to_datetime(ChangedSeconds).

%%---------------------------------------------
%% -spec
%% formatTime(AddMinutes) -> Result,
%% DateTime = datetime(),
%% Result = string().
%%---------------------------------------------

formatTime(DateTime) ->

   {{BaseYears,BaseMonths,BaseDays},{BaseHours,BaseMinutes,BaseSeconds}} = DateTime,

   Years = lists:flatten(io_lib:format("~p",[BaseYears])),
   if
     BaseMonths < 10 -> Months = "0"++lists:flatten(io_lib:format("~p",[BaseMonths]));
     BaseMonths >= 10 -> Months = lists:flatten(io_lib:format("~p",[BaseMonths]))
   end,
   if
     BaseDays < 10 -> Days = "0"++lists:flatten(io_lib:format("~p",[BaseDays]));
     BaseDays >= 10 -> Days = lists:flatten(io_lib:format("~p",[BaseDays]))
   end,
   if
     BaseHours < 10 -> Hours = "0"++lists:flatten(io_lib:format("~p",[BaseHours]));
     BaseHours >= 10 -> Hours = lists:flatten(io_lib:format("~p",[BaseHours]))
   end,
   if
    BaseMinutes < 10 -> Minutes = "0"++lists:flatten(io_lib:format("~p",[BaseMinutes]));
    BaseMinutes >= 10 -> Minutes = lists:flatten(io_lib:format("~p",[BaseMinutes]))
   end,
   if
    BaseSeconds < 10 -> Seconds = "0"++lists:flatten(io_lib:format("~p",[BaseSeconds]));
    BaseSeconds >= 10 -> Seconds = lists:flatten(io_lib:format("~p",[BaseSeconds]))
   end,

   Years++"-"++Months++"-"++Days++" "++Hours++":"++Minutes++":"++Seconds.
